using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Giraffe
{
  class Program
  {
    static void Main(string[] args)
    {
      //string testPhrase = "We are ready!";
      //Console.WriteLine( testPhrase.Length / 2 );
      //Console.WriteLine("Put it in");
      //int myResult = Math.Abs(-60);
      //Console.WriteLine(myResult);

      //Console.Write("Please enter your name:");
      //string name = Console.ReadLine();
      //Console.WriteLine("Hallaisikke " + name);
      //int[] oddNumbers = {1, 3, 5, 7, 9};
      //int i = 0;
      //i++;

      //Console.WriteLine( oddNumbers[0] + oddNumbers[i]);
      //string name = Console.ReadLine();
      //SayHi(name);
      //Console.Write("Enter height in cm: ");
      //double heightInCm = Convert.ToDouble(Console.ReadLine());
      //Console.WriteLine("Your are " + HeightInM(heightInCm) + "m tall.");
      //AgeAssessment();
      //Console.WriteLine("Enter a number between 0 and 6 to be converted to a weekday: ");
      //int index = Convert.ToInt32(Console.ReadLine());
      //Console.WriteLine(GetWeekday(index));

      //int i = 0;
      //while(i < 10)
      //{
      //  i++;
      //  Console.WriteLine("Round " + i);
      //}

      //int j = 6;
      //do
      //{
      //  Console.WriteLine(j);
      //  j++;
      //} while(j < 5);

      //int[] lucky = { 5,6,7,8,9 };
      //for(int i = 0; i < lucky.Length; i++) 
      //{
      //  Console.WriteLine(lucky[i]);
      //}

      //Console.WriteLine(GetPower(5,5));

      //int[,] twoDArray = {
      //  {2,4,6},
      //  {10,20,30}
      //};

      ///*
      //Define empty array with predefined size
      // */
      //int [,] emptyArray = new int[2,2];

      //Console.WriteLine(twoDArray[1,2]);

      //try
      //{
      //  Console.Write("Enter number: ");
      //  int myNumber = Convert.ToInt32(Console.ReadLine());
      //  Console.WriteLine("You chose " + myNumber);
      //}
      //catch(FormatException e)
      //{
      //  Console.WriteLine("Error: "  + e.Message);
      //}
      //finally
      //{
      //  Console.WriteLine("End of program");
      //}

      Book myBook = new Book("Ismael", "Daniel Quinn", 300, 9);
      Book yourBook = new Book("Bienens historie", "Maja Lunde", 460, 3);
      //myBook.title = "Ismael";
      //myBook.author = "Daniel Quinn";
      //myBook.pages = 300;
      myBook.Rating = 5;

      //Console.WriteLine(myBook.Rating);
      //Console.WriteLine(myBook.LongBook());
      Console.WriteLine(Book.bookCount);
    }

    static void SayHi(string name)
    {
      Console.WriteLine(name + ", you are very welcome!");
    }

    static double HeightInM(double heightInCm)
    {
      double newHeight = heightInCm / 100;
      return newHeight;
    }

    static void AgeAssessment()
    {
      Console.Write("Enter your age: ");
      int age = Convert.ToInt32(Console.ReadLine());

      if (age < 30) 
      {
        Console.WriteLine("You are still young");
      } else if (age >= 30 && age < 40)
      {
        Console.WriteLine("Getting older");
      } else
      {
        Console.WriteLine("You are old");
      }
    }

    static string GetWeekday(int index)
    {
      string day;
      switch(index)
      {
        case 0:
          day = "Monday";
          break;
        case 1:
          day = "Tuesday";
          break;
        case 2:
          day = "Wednesday";
          break;
        case 3:
          day = "Thursday";
          break;
        case 4:
          day = "Friday";
          break;
        case 5:
          day = "Saturday";
          break;
        case 6:
          day = "Sunday";
          break;
        default:
          day = "only numbers from 0 to 6";
          break;
      }
      
      return day;
    }

    static int GetPower(int baseNum, int powerNum)
    {
      int result = 1;
      for(int i = 0; i < powerNum; i++)
      {
        result = result * baseNum;
      }
      return result;
    }
  }
}
