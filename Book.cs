using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Giraffe
{
  class Book
  {
    public string title;
    public string author;
    public int pages;
    private int rating;
    //public int rating;
    public static int bookCount = 0; 

    // Constructors

    public Book(){} // Used for empty books

    public Book(string aTitle, string aAuthor, int aPages, int aRating) // Used for books defined during initialization
    {
      title = aTitle;
      author = aAuthor;
      pages = aPages;
      Rating = aRating;
      bookCount++;
    }

    // Methods
    
    public bool LongBook()
    {
      if (pages > 100)
      {
        return true;
      } else {
        return false;
      }
    }

    // Getter and Setter for rating

    public int Rating
    {
      get
      {
        return rating;
      }
      set
      {
        if (value >= 0 && value <= 5)
        {
          rating = value;
        } else {
          rating = 0;
        }
      }
    }
  }
}
